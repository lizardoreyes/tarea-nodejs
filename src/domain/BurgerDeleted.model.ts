import mongoose from "mongoose";

export interface BurgerDeletedI extends mongoose.Document {
    _id?: string;
    name: string;
    description: string;
    image: string;
    price: number;
    burgers_details: BurgerDetailsDeletedI;
}

export interface BurgerDetailsDeletedI extends mongoose.Document {
    _id?: string;
    calories: string;
    proteins: string;
    carbohydrates: string;
    sugars: string;
    grease: string;
    saturated_fat: string;
    trans_fat: string;
    sodium: string;
}
