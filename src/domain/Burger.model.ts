import mongoose from "mongoose";

export interface BurgerI extends mongoose.Document {
    _id?: string;
    name: string;
    description: string;
    image: string;
    price: number;
    burgers_details: BurgerDetailsI;
}

export interface BurgerDetailsI extends mongoose.Document {
    _id?: string;
    calories: string;
    proteins: string;
    carbohydrates: string;
    sugars: string;
    grease: string;
    saturated_fat: string;
    trans_fat: string;
    sodium: string;
}
