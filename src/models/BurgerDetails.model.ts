import { Schema, model, SchemaTypes } from "mongoose";

const BurgerDetailsSchema = new Schema({
    calories: {
        type: String,
        required: true
    },
    proteins: {
        type: String,
        required: true
    },
    carbohydrates: {
        type: String,
        required: true
    },
    sugars: {
        type: String,
        required: true
    },
    grease: {
        type: String,
        required: true
    },
    saturated_fat: {
        type: String,
        required: true
    },
    trans_fat: {
        type: String,
        required: true
    },
    sodium: {
        type: String,
        required: true
    },
});

export default model("burgers-details", BurgerDetailsSchema);
