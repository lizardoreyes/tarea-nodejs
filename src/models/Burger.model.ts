import { Schema, model } from "mongoose";

const BurgerSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    burgers_details: {
        type: Schema.Types.ObjectId,
        ref: 'burgers-details',
        required: true
    }
});

export default model("burgers", BurgerSchema);
