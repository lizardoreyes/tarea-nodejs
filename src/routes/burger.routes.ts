// path - ruta : api/dishes
import { Router } from "express";
import { create, getAll, getById, update, deleteBurger, getAllDeleted } from "../controllers/burger.controller";
import { check } from "express-validator";
import { fieldValidator, idValidator } from "../middlewares/burger.middleware";
import validateJWT from "../middlewares/validateJWT.middleware";

const router = Router();

// Solo para pruebas
// Solo usuarios autenticados pueden ver la lista de hamburguesas eliminadas
router.get("/deleted", validateJWT, getAllDeleted);

router.get("/", getAll);
router.get("/:id", idValidator, getById);
router.patch("/", [validateJWT, idValidator], update);
router.delete("/", [validateJWT, idValidator], deleteBurger);

router.post("/", [
    validateJWT,
    check("name", "El campo \"name\" es requerido").notEmpty(),
    check("description", "El campo \"description es requerido").notEmpty(),
    check("image", "El campo \"image\" es requerido").notEmpty(),
    check("price", "El campo \"price\" es requerido").notEmpty(),
    check("calories", "El campo \"calories\" es requerido").notEmpty(),
    check("proteins", "El campo \"proteins\" es requerido").notEmpty(),
    check("carbohydrates", "El campo \"carbohydrates\" es requerido").notEmpty(),
    check("sugars", "El campo \"sugars\" es requerido").notEmpty(),
    check("grease", "El campo \"grease\" es requerido").notEmpty(),
    check("saturated_fat", "El campo \"saturated_fat\" es requerido").notEmpty(),
    check("trans_fat", "El campo \"trans_fat\" es requerido").notEmpty(),
    check("sodium", "El campo \"sodium\" es requerido").notEmpty(),
    fieldValidator
], create);

export default router;
