// path - ruta : api/users

import { Router } from 'express';
import { listUsers, listUser, createUser } from "../controllers/user.controller";

const router = Router();

router.get("/", listUsers);
router.get("/:id", listUser);
router.post("/", createUser);

export default router;