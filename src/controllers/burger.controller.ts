import { Response, Request } from "express";
import Burger from "../models/Burger.model";
import BurgerDetails from "../models/BurgerDetails.model";
import BurgerDeleted from "../database/Logic/BurgerDeleted.model";
import BurgerDetailsDeleted from "../database/Logic/BurgerDetailsDeleted.model";
import { networkError, networkSuccess, serverError } from "../middlewares/response.middleware";
import { BurgerDetailsI, BurgerI } from "../domain/Burger.model";
import { BurgerDeletedI, BurgerDetailsDeletedI } from "../domain/BurgerDeleted.model";

// LIST BURGER
const getAll = async (req: Request, res: Response) => {

    try {
        const burgers = await Burger.find().populate("burgers_details") as BurgerI[];
        networkSuccess(res, burgers, "Listado de Hamburguesas")
    } catch (error) {
        serverError(res, error)
    }
}

// LIST BURGER
const getById = async (req: Request, res: Response) => {

    try {
        const burger = await Burger.findById(req.params.id).populate("burgers_details") as BurgerI;
        if(burger) {
            return networkSuccess(res, burger, "Hamburguesa encontrada")
        }

        networkError(res, "Hamburguesa no encontrada", "No se ha encontrado la hamburguesa")

    } catch (error) {
        serverError(res, error)
    }
}

// CREATE BURGER
const create = async (req: Request, res: Response) => {

    try {
        const { image, price, name, description } = req.body as BurgerI
        const { calories, proteins, carbohydrates, sugars, grease, saturated_fat, trans_fat, sodium } = req.body as BurgerDetailsI

        const burgerDetails = new BurgerDetails({ calories, proteins, carbohydrates, sugars, grease, saturated_fat, trans_fat, sodium }) as BurgerDetailsI
        await burgerDetails.save()

        const burger = new Burger({ image, price, name, description }) as BurgerI;
        // Enlazamos los "detalles de la hamburguesa" con la "hamburguesa"
        burger.burgers_details = burgerDetails;
        await burger.save();

        networkSuccess(res, burger, "Hamburguesa creada correctamente", 201);

    } catch (error) {
        serverError(res, error)
    }
}

// UPDATE BURGER
const update = async (req: Request, res: Response) => {

    try {
        const { calories, proteins, carbohydrates, sugars, grease, saturated_fat, trans_fat, sodium } = req.body as BurgerDetailsI
        const burgerDetails = { calories, proteins, carbohydrates, sugars, grease, saturated_fat, trans_fat, sodium } as BurgerDetailsI

        const { _id, image, price, name, description } = req.body as BurgerI
        const burger = { _id, name, description, image, price } as BurgerI

        const burgerSave = await Burger.findOneAndUpdate({ _id: burger._id }, burger, { new: true }).populate("burgers_details") as BurgerI;
        await BurgerDetails.updateOne({ _id: burgerSave.burgers_details._id }, burgerDetails);

        networkSuccess(res, burgerSave, "Hamburguesa actualizada correctamente", 201);

    } catch (error) {
        serverError(res, error)
    }
}

// DELETE BURGER
const deleteBurger = async (req: Request, res: Response) => {

    const id = req.body._id;

    try {
        // Eliminamos la hamburguesa y todos sus detalles
        const burgerDeleted = await Burger.findOneAndDelete({ _id: id }) as BurgerI;
        const burgerDetailsDeleted = await BurgerDetails.findOneAndDelete({ _id: burgerDeleted.burgers_details._id }) as BurgerDetailsI;

        if(!burgerDeleted) {
            return networkError(res, "Hamburguesa no encontrada", "No se ha encontrado la hamburguesa", 404)
        }

        const { _id, name, description, image, price } = burgerDeleted as BurgerI;
        const { calories, proteins, carbohydrates, sugars, grease, saturated_fat, trans_fat, sodium } = burgerDetailsDeleted as BurgerDetailsI;

        // Guardamos la hamburguesa y sus detalles eliminados en la tabla de "eliminados"
        const burgerDetailsDeletedSave = await BurgerDetailsDeleted.create({ calories, proteins, carbohydrates, sugars, grease, saturated_fat, trans_fat, sodium }) as BurgerDetailsDeletedI;
        await BurgerDeleted.create({ _id, name, description, image, price, burgers_details: { _id: burgerDetailsDeletedSave._id } });

        networkSuccess(res, "Hamburguesa eliminada", "Hamburguesa eliminada correctamente", 201)

    } catch (error) {
        serverError(res, error)
    }
}

// EXTRA: SHOW DELETED BURGERS - ONLY TESTING - NO PRODUCTION!
const getAllDeleted = async (req: Request, res: Response) => {

    try {
        const burgersDeleted = await BurgerDeleted.find().populate("burgers_details") as BurgerDeletedI[];
        networkSuccess(res, burgersDeleted, "Listado de Hamburguesas Eliminadas")
    } catch (error) {
        serverError(res, error)
    }
}


export { getAll, getById, create, update, deleteBurger, getAllDeleted };
