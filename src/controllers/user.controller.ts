import bcrypt from "bcryptjs"
import User from "../models/User.model"
import { UserI } from "../domain/User.model";

// LIST USERS
const listUsers = async (req, res) => {
    try {
        const users = await User.find() as UserI[];
        return res.status(200).send({
            success: true,
            users
        });
    } catch (error) {
        res.status(500).send({
            error,
            success: false,
            message: "Ha ocurrido un problema"
        });
    }
}

// LIST USER
const listUser = async (req, res) => {
    const { id } = req.params

    try {
        const user = await User.findById(id) as UserI;
        return res.status(200).send({
            success: true,
            user
        });
    } catch (error) {
        res.status(500).send({
            error,
            success: false,
            message: "Ha ocurrido un problema"
        });
    }
}

// CREATE USER
const createUser = async (req, res) => {
    try {
        const { email, name, lastName, password } = req.body as UserI;
        if (email && name && lastName && password) {

            const salt = bcrypt.genSaltSync();
            const user = new User(req.body) as UserI;

            user.password = bcrypt.hashSync(password, salt);

            await user.save();
            return res.status(201).send({
                success: true,
                message: "Usuario creado correctamente."
            });
        }
        return res.status(400).send({
            success: false,
            message: "Datos invalidos"
        });
    } catch (error) {
        res.status(500).send({
            error,
            success: false,
            message: "Ha ocurrido un problema"
        });
    }
}

export { listUsers, listUser, createUser };
