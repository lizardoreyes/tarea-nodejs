import bcrypt from 'bcryptjs';
import { Request, Response } from "express";
import User from '../models/User.model';
import generateJWT from "../utils/generateJWT.utils";
import { UserI } from "../domain/User.model";
import { networkError, networkSuccess, serverError } from "../middlewares/response.middleware";

const login = async (req: Request, res: Response) => {
    try {
        const { email, password } = req.body;
        const userDb = await User.findOne({ email }) as UserI;

        if(!userDb) {
            return networkError(res, "Usuario no existe", "Usuario no existe", 404)
        }

        const validPassword = bcrypt.compareSync(password, userDb.password);
        if(!validPassword) {
            return networkError(res, "Usuario o contraseña incorrectos", "Usuario o contraseña incorrectos")
        }

        const token = await generateJWT(userDb._id) as string;
        networkSuccess(res, { token } , "Usuario logeado correctamente")

    } catch(error) {
        serverError(res, error)
    }
}

// TODO - Utilizar el renovar token
const renewToken = async (req: Request, res: Response) => {
    try {
        //@ts-ignore
        const uid = req.uid as string;
        const token = await generateJWT(uid);
        const user = await User.findById(uid) as UserI;

        networkSuccess(res, { token, user }, "Token actualizado");

    } catch (err) {
        serverError(res, err)
    }
}

export { login }
