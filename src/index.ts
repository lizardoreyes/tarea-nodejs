import express from "express";
import cors from "cors";
import { config } from "dotenv";
import dbConnection from "./database/config";

// Routes - Import
import userRoutes from "./routes/user.routes";
import authRoutes from "./routes/auth.routes";
import burgerRoutes from "./routes/burger.routes";

config();
dbConnection();

const PORT: number = parseInt(process.env.PORT) | 3000;
const app = express();

app.use(cors());
app.use(express.json());

// Routes - Use
app.use("/api/users", userRoutes);
app.use("/api/burgers", burgerRoutes);
app.use("/api/auth", authRoutes);

app.listen(PORT, () => {
    console.log(`Servidor iniciado en el puerto ${PORT}`);
});
