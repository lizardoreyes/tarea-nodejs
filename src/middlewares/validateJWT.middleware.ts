import jwt from 'jsonwebtoken';
import { Request, Response } from 'express';
import {networkError, serverError} from "./response.middleware";

const validateJWT = (req: Request, res: Response, next: () => void) => {
    try {
        const token = req.headers.authorization;
        if (!token) {
            return networkError(res, {}, "Faltan las credenciales", 401);
        }

        validation(token, req, res, next);

    } catch (err) {
        serverError(res, err);
    }
}

const validation = (token: string, req: Request, res: Response, next: () => void) => {
    try {
        console.log(jwt.verify(token, process.env.JWT_KEY));
        // @ts-ignore
        const { uid } = jwt.verify(token, process.env.JWT_KEY);
        // @ts-ignore
        req.uid = uid;

        next();
    } catch (err) {
        networkError(res, err, "Token no valido");
    }
}

export default validateJWT;
